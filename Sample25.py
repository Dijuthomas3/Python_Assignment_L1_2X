# Write a program to print the different data types( Numbers, strings characters) using the Format symbols

a=raw_input("Enter any alphabet: ")
num1=input("Enter a number: ")
print "The alphabet entered is %c" %a
print "The another alphabet is %s" %a
print "The signed int value is %d" %num1
print "The signed int value is %i" %num1
print "The unsigned integer is %u" %num1
print "The octal integer is %o" %num1
print "The hexadecimal integer is %x" %num1
print "The hexadecimal integer is %X" %num1
print "The exponential notation is %e" %num1
print "The exponential notation is %E" %num1
print "The floating point real number is %f" %num1
print "The the shorter of f and g is %g" %num1
print "The the shorter of f and G is %G" %num1
