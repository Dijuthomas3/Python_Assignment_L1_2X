# Read the value x and y from the user and apply all trigonometric functions on these numbers.

p=input("Enter the angle in radians: ")
q=input("Enter the angle in radians: ") 
import math
x=(180/(math.pi))*(p) 
print "Angle x in degrees: ",x
y=(180/(math.pi))*(q)
print "Angle y in degrees: ",y
sine=math.sin(p) 
sine2=math.sin(q) 
cosine=math.cos(p) 
cosine2=math.cos(q) 
tangent=math.tan(p) 
tangent2=math.tan(q) 
cotangent=math.atan(p) 
cotangent2=math.atan(q) 
cosecant=math.asin(p) 
cosecant2=math.asin(q) 
secant=math.acos(p) 
secant2=math.acos(q) 
o=math.atan2(q,p) 
print "sine of angle x is: ",sine
print "cos of angle x is: ",cosine
print "tan of angle x is: ",tangent
print "asine of angle x is: ",cosecant
print "acos of angle x is: ",secant
print "atan of angle x is: ",cotangent
print "sine of angle y is: ",sine2
print "cos of angle y is: ",cosine2
print "tan of angle y is: ",tangent2
print "asine of angle y is: ",cosecant2
print "acos of angle y is: ",secant2
print "atan of angle y is: ",cotangent2
print "tan(y/x) of angle x and y is: ",0
