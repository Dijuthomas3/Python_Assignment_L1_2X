# Create 3 dictionaries(dict1,dict2,dict3) with 3 elements each. Perform following operations
# a) Compare dictionaries to determine the biggest.
# b) Add new elements in to the dictionaries dict1, dict2 
# c) print the length of dict1,dict2,dict3.
# d) Convert dict1, dict2, and dict3 dictionaries as string and concatenate all strings together. 

dict1={'Name':'DijuThomas','Age':30,'salary':400000}
dict2={'Company':'Wipro','Location':'EC','city':'kerala'}
dict3={'course':'Python','webex':'yes','attendance':'necessary'}
print "dict1: ", dict1
print "dict2: ", dict2
print "dict3: ", dict3
print "dict1 compared to dict2 is: ", cmp(dict1,dict2)
print "dict2 compared to dict3 is: ", cmp(dict2,dict3)
print "dict1 compared to dict3 is: ", cmp(dict1,dict3)
dict1['friend'] = 'Arnab'
print "dict1 after appending: ", dict1
print "length of dict1, dict2, dict3 is: ", len(dict1),len(dict2),len(dict3)
list1=str(dict1)
list2=str(dict2)
list3=str(dict3)
list4=list1+list2+list3
print "concatenated dicts as string: ", list4

