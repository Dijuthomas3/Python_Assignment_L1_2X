#Explore all escape characters


str1="my name is\a Dijuthomas"
print "The escape character that shows alert or bell is:",str1
str1="my name is Diju\bthomas"
print "The escape character that shows backspace is:",str1
str1="my name\cx is Dijuthomas"
print "The escape character that shows control=x is:",str1
str1="my name\e is Dijuthomas"
print "The escape cgaracter that shows escape is:",str1
str1="my name\n is Dijuthomas"
print "The escape character that shows newline character is:",str1
str1="my name is Diju\sthomas"
print "The escape character that shows space character is:",str1
str1="my name is Diju\tthomas"
print "The escape character that shows tab is:",str1
str1="my name is Diju\rthomas"
print "The escape character that shows carriage return is:",str1
str1="my name is Diju\vthomas"
print "The escape charactyer that shows vertical tab is:",str1
str1="my name is Dijuthomas\nnn"
print "The escape character that shows octal notation:",str1
