#Create 2 dictionaries as follows
#   dict1 ={'Name':'Ramakrishna','Age':25}
#   dict2={'EmpId':1234,'Salary':5000}
#      Perform following operations   
#        a) Create single dictionary by merging dict1 and dict2
#        b) Update the salary to 10%
#        c) Update the age to 26
#        d) Insert the new element with key "grade" and assign value as "B1"
#        e) Extract and print all values and keys separately.
#        f) delete the element with key 'Age' and print dictionary elements.


dict1 ={'Name':'Ramakrishna','Age':25}
dict2={'EmpId':1234,'Salary':5000}



dict1.update(dict2)
print "merging 2 dictionaries and printing the values:",dict1


dict1['Salary']=5000+(5000*0.1)
dict1['Age']=26
print "Updated salary: ",dict1['Salary']
print "Updated Age: ",dict1['Age']



dict1['Grade']='B1'
print "added key 'grade' to dict1:",dict1
print 'all values and keys are seperated:'
list1=list(dict1)
n=len(list1)
for i in range(n):
    p=list1[i]
    print list1[i],dict1[p]


del dict1['Age']
print "printing the elements from dict1 after deleting 'Age';",dict1
