# Using the built in functions on Numbers perform following operations 
# a) Round of the given floating point number example : n=0.543 then round it next decimal number , i.e n=1.0 Use round() function 
# b) Find out the square root of a given number. ( use sqrt(x) function)
# c) Generate random number between 0, and 1 ( use random() function)
# d) Generate random number between 10 and 500. ( use uniform() function) 
# e) Explore all Math and Random module functions on a given number/ List. ( Refer to tutorial for Math & Random functions list) 

n= input("Enter the number:")
import math 
import random 
s= round(n,2)
print "math.round:", s
p= math.sqrt(n)
print "square root of number: ", p
q= random.random()
print "random no b/w 0 and 1:", q
r= random.uniform(10,500)
print "random b/w 10 and 500 using uniform:", r
