# Create a list with 7 elements and perform following operations:
#   a) Append an object 80 to the List
#   b) insert object 100 at  4th position
#   c) Sort the list and print all elements
#   d) Sort the elements of the list in descending order.
#   e) delete last three elements using pop operation

list1=input("Enter values in list1: ")
n=input("Enter the value to append: ")
m=input("Enter the value to insert at index 4: ")
list1.append(n)
print "After appending 80: ", list1
list1.insert(4,m)
print "After inserting 100: ", list1
list1.sort()
print "Sorted list: ", list1
list1.reverse()
print "Sorted in descending order: ", list1
list1.reverse()
a=list1.pop()
b=list1.pop()
c=list1.pop()
print "Removed elements using pop: ", a,b,c
print "Final list: ", list1
